# Description des différentes étapes

## Initialisation du projet

- Utilisation d'un projet template en React trouvé sur la documentation de **npm**.  
  `npm init react-app ./my-react-app`
- Mise à jour du script de test pour que les tests soient lancés automatique sans passer par des inputs claviers.  
  `"react-scripts test --watchAll"`

## Configuration de Docker

- Création d'un fichier Dockerfile pour la configuration de l'image Docker
- Création d'un fichier .dockerignore pour ignorer les fichiers inutiles lors de la construction de l'image Docker
- Exécution de la commande `docker build -t sample:dev .` pour construire l'image Docker
- Exécution de la commande `docker run -it -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm sample:dev` pour lancer l'application en mode développement

## Configuration de GitLab CI/CD

- Ajout de **3 jobs**
  - Build de l'application (**stage:** build)
  - Lancement des tests (**stage:** test)
  - Build du docker (**stage:** docker-build)
- Mise en **cache** des node_modules pour éviter de tout re-télécharger à chaque fois.
- Dans le docker-build, on a ajouté la clé **only** avec la valeur **tag** pour ne lancer le docker-build uniquement lors du lancement d'un tag.
